# App del Clima

Es una web app desarrollada en angular version 7.3.2. que muestra el clima de acuerdo a la localizacion y una lista de ciudades con su respectiva información.

## Instalación

Use el gestor de paquetes [npm](https://www.npmjs.com/) para instalar las dependencias.

```bash
npm install
```

## Uso

Ejecute en la consola de comando `ng serve --open` y Navegar a la url http://localhost:4200 
La aplicación se volverá a cargar automáticamente si cambia alguno de los archivos de origen.

## License
[MIT](https://choosealicense.com/licenses/mit/)
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { WeatherService } from '../../shared/services/weather.service';
import { Weather } from '../../shared/models/weather';
import { City } from '../../shared/interfaces/city';
import { Forecast } from '../../shared/models/forecast';

@Component({
  selector: 'app-current-weather',
  templateUrl: './current-weather.component.html',
  styleUrls: ['./current-weather.component.css']
})
export class CurrentWeatherComponent implements OnInit {

  weather: Weather;
  city: City;
  forecasts: Forecast[] = [];

  constructor(private weatherService: WeatherService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.data.subscribe(response => { 
      this.weather = response.data;
      this.city = {city: this.weather.city, code: this.weather.code}
      this.weatherService.getForecast(this.city).then((response: Forecast[]) => {
        this.forecasts = response;
      },
      error => {
        console.log(error);
      });
    });
  }

}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard.component';
import { WeatherComponent } from './weather/weather.component';
import { WeatherResolveService } from '../shared/services/weather.resolve.service';
import { CurrentWeatherResolveService } from '../shared/services/current.weather.resolve.service';
import { CurrentWeatherComponent } from './current-weather/current-weather.component';
import { DetailCityComponent } from './detail-city/detail-city.component';


const routes: Routes = [
  { 
    path: '',
    component: DashboardComponent,
    resolve: { data: WeatherResolveService },
    children: [
      {
        path: 'current-weather',
        component: CurrentWeatherComponent,
        resolve: { data: CurrentWeatherResolveService }
      },
      {
        path: 'detail-city',
        component: DetailCityComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }

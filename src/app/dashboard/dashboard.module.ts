import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { WeatherComponent } from './weather/weather.component';
import { CurrentWeatherComponent } from './current-weather/current-weather.component';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import { WeatherResolveService } from '../shared/services/weather.resolve.service';
import { CurrentWeatherResolveService } from '../shared/services/current.weather.resolve.service';
import { DetailCityComponent } from './detail-city/detail-city.component';

@NgModule({
  declarations: [
    DashboardComponent,
    WeatherComponent,
    CurrentWeatherComponent,
    DetailCityComponent
  ],
  imports: [
    CommonModule,
    DashboardRoutingModule
  ],
  providers: [
    WeatherResolveService,
    CurrentWeatherResolveService
  ]
})
export class DashboardModule { }

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { WeatherService } from '../../shared/services/weather.service';
import { Weather } from '../../shared/models/weather';
import { City } from '../../shared/interfaces/city';
import { Forecast } from '../../shared/models/forecast';

@Component({
  selector: 'app-detail-city',
  templateUrl: './detail-city.component.html',
  styleUrls: ['./detail-city.component.css']
})
export class DetailCityComponent implements OnInit {

  weather: any;
  city: City;
  forecasts: Forecast[] = [];

  constructor(public route: ActivatedRoute, private weatherService: WeatherService) { }

  ngOnInit() {
    this.weather = JSON.parse(window.localStorage.getItem('weather'));
    this.city = {city: this.weather.city, code: this.weather.code}
    this.weatherService.getForecast(this.city).then((response: Forecast[]) => {
      this.forecasts = response;
    },
    error => {
      console.log(error);
    });
  }

}

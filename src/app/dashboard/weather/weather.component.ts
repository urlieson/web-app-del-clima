import { Component, OnInit, NgZone } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { WeatherService } from '../../shared/services/weather.service';
import { Weather } from '../../shared/models/weather';


@Component({
  selector: 'app-weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.css']
})
export class WeatherComponent implements OnInit {

  weathers: Weather[] = [];

  constructor(private weatherService: WeatherService, private route: ActivatedRoute, private router: Router, private ngZone:NgZone) { }

  ngOnInit() {
    console.log('init weather');
    this.route.data.subscribe(response => { 
      this.weathers = response.data;
    });

  }

  detailCity(weather){
    window.localStorage.setItem('weather', JSON.stringify(weather));
    this.ngZone.run(() => {
      this.router.navigate(['/dashboard/detail-city']);
    });
  }

}

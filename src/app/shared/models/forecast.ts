export class Forecast {
    constructor( public nextDate: string,
        public nextDay: string,
        public nextHour: string,
        public nextTemp: string,
        public nextDescription: string,
        public nextWind:string,
        public nextImg:string    
    ){}
}
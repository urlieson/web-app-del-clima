export class Weather {
    constructor(public city:string,
                public code: string,
                public description: string,
                public temp:string,
                public wind:string,
                public img:string){}
}
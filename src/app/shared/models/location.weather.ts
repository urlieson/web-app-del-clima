export class LocationWeather {
    constructor(public code: string,
                public lat:string,
                public lon: string){}
}
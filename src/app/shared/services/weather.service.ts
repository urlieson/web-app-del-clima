import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { City } from '../interfaces/city';
import { Weather } from '../models/weather';
import { Forecast } from '../models/forecast';
import { LocationWeather } from '../models/location.weather';
import 'rxjs/Rx';
import { map } from 'rxjs/operators';
import 'rxjs/add/operator/toPromise';
import { MapsAPILoader } from '@agm/core';

declare const google: any;

@Injectable({
  providedIn: 'root'
})
export class WeatherService {
  lang = "es";
  location;
  weathers: any[] = [];
  weather: Weather;
  locationWeather: LocationWeather;
  weatherUrl = "assets/weather.json";
  appId = "3d8b309701a13f65b660fa2c64cdc517";
  nextHours = 8;

  days = ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sábado'];
  options = { year: 'numeric', month: 'long', day: 'numeric' };
  times = new Array();
  posDay = new Array();  
  forecasts: Forecast[] = [];
  forecast: Forecast;

  constructor(private http: HttpClient, private mapsAPILoader: MapsAPILoader) { }

  // get all weather in the file weather.json
  getWeathers() {
    return new Promise((resolve, reject) => {
      this.http.get(this.weatherUrl).toPromise().then(
        (response: City[]) => {
          response.map(item => {
            return this.getWeather(item).then(response => {
              this.weathers.push(response);
            },
            error => {
              console.log(error);
            });
          });
          resolve(this.weathers);
        },
        (error) => {
          console.log(error);
          reject(error);
        }
      );
    });
  }

  // get weather for city and code
  getWeather(city: City) {
    let url = `https://api.openweathermap.org/data/2.5/weather?units=metric&lang=${this.lang}&q=${city.city},${city.code}&appid=${this.appId}`;
    return new Promise((resolve, reject) => {
      this.http.get(url).toPromise().then(response => {
        this.weather = new Weather(response['name'], city.code, response['weather'][0]['description'], response['main']['temp'], response['wind']['speed'], response['weather'][0]['icon']);
        resolve(this.weather);
        return this.weather;
      },
      error => {
        reject(error);
      });
    })
  }

  // get current location
  getCurrentLocation(){

    return new Promise((resolve, reject) => {
      this.getLocation().then(response => {
        const code = response['code'];
        return this.http.get(`https://api.openweathermap.org/data/2.5/weather?lang=${this.lang}&appid=${this.appId}&lat=${response['lat']}&lon=${response['lon']}&units=metric`).toPromise().then(
          (response: City) => {
            this.weather = new Weather(response['name'], code, response['weather'][0]['description'], response['main']['temp'], response['wind']['speed'], response['weather'][0]['icon']);
            resolve(this.weather);
            return this.weather;
          }
        );
      },
      error => {
        console.log(error);
        reject(error);
      });
    });
  }

  getLocation(){
    return new Promise((resolve, reject)=>{

      navigator.geolocation.getCurrentPosition((pos) => {
        this.location = pos.coords;

        const lat = this.location.latitude;
        const lon = this.location.longitude;

        this.mapsAPILoader.load().then(() => {
          const geocoder = new google.maps.Geocoder();
          const latlng = new google.maps.LatLng(lat, lon);
          const request = { latLng: latlng };
          geocoder.geocode(request, (results, status) => {
            if (status === google.maps.GeocoderStatus.OK) {
              let address_components = results[0].address_components;
              let address = address_components.filter(r=>{
                if(r.types[0] == 'country'){
                  return r;
                }
              }).map(r=>{
                return r.short_name;
              })

              this.locationWeather = new LocationWeather(address[0], lat, lon);
              resolve(this.locationWeather);
              return this.locationWeather;
            }
          });
        });
      });
    });
  }

  getForecast(city: City){
    let url = `https://api.openweathermap.org/data/2.5/forecast?units=metric&lang=${this.lang}&q=${city.city},${city.code}&appid=${this.appId}&cnt=${this.nextHours}`;
    return new Promise((resolve, reject) => {
      this.http.get(url).toPromise().then(response => {
      
        for (var i = 0; i < response['list'].length; i++){
          this.forecast = new Forecast(
            new Date(response['list'][i]['dt_txt']).toLocaleDateString("es-ES", this.options),
            this.days[new Date(response['list'][i]['dt_txt']).getDay()],
            this.tConvert(response['list'][i]['dt_txt'].split(' ')[1]),
            response['list'][i]['main']['temp'],
            response['list'][i]['weather'][0]['description'],
            response['list'][i]['wind']['speed'],
            response['list'][i]['weather'][0]['icon'],
          );

          this.forecasts[i] = this.forecast;
          
        }
        resolve(this.forecasts);
      },
      error => {
        reject(error);
      });
    })
  }

  tConvert (time) {
    // Check correct time format and split into components
    time = time.toString ().match (/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];
 
     if (time.length > 1) { // If time format correct
       time = time.slice (1);  // Remove full string match value
       time[5] = +time[0] < 12 ? 'AM' : 'PM'; // Set AM/PM
       time[0] = +time[0] % 12 || 12; // Adjust hours
     }

     return time.join ('');
  }
}